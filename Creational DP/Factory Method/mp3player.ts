import {AudioPlayer, AudioPlayerCreator} from "./interfaces";

export class MP3Player implements AudioPlayer {
    pause(): void {
        console.log('mp3 pause')
    }

    resume(): void {
        console.log('mp3 resume')
    }

    run(): void {
        console.log('mp3 run')
    }

    stop(): void {
        console.log('mp3 stop')
    }

}

export class MP3PlayerCreator implements AudioPlayerCreator {
    create(): AudioPlayer {
        return new MP3Player();
    }
}