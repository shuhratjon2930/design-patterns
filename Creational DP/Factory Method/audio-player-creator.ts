import {MP3PlayerCreator} from "./mp3player";
import {OggPlayerCreator} from "./oggplayer";

/**
 * @enum {string}
 */
export type AudioPlayerType = 'mp3' | 'ogg'

/**
 * Audio player factory method class
 */
export class AudioPlayerFactory{
    /**
     * Creates concreate player
     * @param {AudioPlayerType} playerType
     */
    static create(playerType: AudioPlayerType) {
        switch (playerType) {
            case "mp3": return new MP3PlayerCreator().create();
            case "ogg": return new OggPlayerCreator().create();
        }
    }
}