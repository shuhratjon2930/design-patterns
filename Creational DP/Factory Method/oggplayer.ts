import {AudioPlayer, AudioPlayerCreator} from "./interfaces";

export class OggPlayer implements AudioPlayer {
    pause(): void {
        console.log('ogg pause')
    }

    resume(): void {
        console.log('ogg resume')
    }

    run(): void {
        console.log('ogg run')
    }

    stop(): void {
        console.log('ogg stop')
    }
}

export class OggPlayerCreator implements AudioPlayerCreator {
    create(): AudioPlayer {
        return new OggPlayer();
    }
}