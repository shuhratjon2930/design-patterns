export interface AudioPlayer {
    run(): void;
    stop(): void;
    pause(): void;
    resume(): void;
}

export interface AudioPlayerCreator {
    create(): AudioPlayer;
}