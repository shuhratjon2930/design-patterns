import {AudioPlayerFactory} from "./audio-player-creator";

function main() {
    //    Test One
    const mp3 = AudioPlayerFactory.create('mp3');
    mp3.run();
    mp3.pause();
    mp3.resume();
    mp3.stop();
    // Test two
    const ogg = AudioPlayerFactory.create('ogg');
    ogg.run();
    ogg.pause();
    ogg.resume();
    ogg.stop();
}

main();